<?php

use Illuminate\Support\Str;
use Faker\Generator as Faker;
use Illuminate\Database\Seeder;

class ProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        $data = [];
        for($i = 0; $i < 10; $i++) {
            $data[] =[
                'first_name'        =>  $faker->firstName,
                'last_name'         =>  $faker->lastName,
                'email'             =>  $faker->unique()->safeEmail,
                'phone'             =>  $faker->phoneNumber,
                'position'          =>  $faker->jobTitle,
                'summary'           =>  $faker->company,
                'skills'            =>  $faker->catchPhrase,
                'education'         =>  $faker->company,
                'work_experience'   =>  $faker->sentence(6,true),
                'additional_info'   =>  $faker->sentence(8,true),
                'created_at'        =>  \Carbon\Carbon::now(),
                'updated_at'        =>  \Carbon\Carbon::now(),
            ];
        }
        App\Profile::insert($data);
    }
}
