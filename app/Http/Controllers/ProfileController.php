<?php

namespace App\Http\Controllers;

use PDF;
use App\Profile;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profiles = Profile::all();
        return view('profiles.list',['profiles'=> $profiles]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $fields = [
            'first_name' => [
                'title' =>  'First Name',
                'type'  =>  'input'
            ],
            'last_name'  => [
                'title' =>  'Last Name',
                'type'  =>  'input'
            ],
            'email'  => [
                'title' =>  'Email',
                'type'  =>  'input'
            ],
            'phone'  => [
                'title' =>  'Phone',
                'type'  =>  'input'
            ],
            'position'  => [
                'title' =>  'Position',
                'type'  =>  'input'
            ],
            'summary'  => [
                'title' =>  'Summary',
                'type'  =>  'textarea'
            ],
            'skills'  => [
                'title' =>  'Skills',
                'type'  =>  'textarea'
            ],
            'education'  => [
                'title' =>  'Education',
                'type'  =>  'textarea'
            ],
            'work_experience'  => [
                'title' =>  'Work Experience',
                'type'  =>  'textarea'
            ],
            'additional_info'  => [
                'title' =>  'Additional Info',
                'type'  =>  'textarea'
            ]
        ];
        return view('profiles.create', compact('fields'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name'    =>  'required',
            'last_name'     =>  'required',
            'email'         =>  'required|email',
            'phone'         =>  'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
            'position'      =>  'required',
            'summary'       =>  'required',
            'skills'        =>  'required',
            'education'     =>  'required',
            'work_experience'   =>  'required',
            'additional_info'   =>  'required',
         ]);
        $profile = Profile::create($request->all());
        return redirect('profiles');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function show(Profile $profile)
    {
        return response()->json($profile);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function edit(Profile $profile)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Profile $profile)
    {
        $profile->update($request->all());
        return response()->json($profile);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profile $profile)
    {
        $profile->delete();
        return response()->json(['status'=>true]);
    }
    
    public function createPDF() {
      // retreive all records from db
      $data = Profile::all();

      // share data to view
      view()->share('profiles',$data);
      $pdf = PDF::loadView('profiles.pdf_view', $data);

      // download PDF file with download method
      return $pdf->download('pdf_file.pdf');
    }
}
