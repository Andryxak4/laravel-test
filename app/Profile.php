<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = [
        'first_name', 'last_name', 'email', 'phone', 'position', 'summary', 'skills', 'education', 'work_experience', 'additional_info'
    ];
}
