@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Profiles</div>
                <div class="card-body">
                    <form method="POST" action="/profiles">
                        @method('POST')
                        @csrf
                        
                        @foreach ($fields as $name => $field)
                            <div class="form-group">
                                <label for="{{$name}}">{{$field['title']}}</label>
                                @if ($field['type'] == 'input')
                                    <input class="form-control" name='{{$name}}' id="{{$name}}" aria-describedby="{{$name}}Help" placeholder="Enter {{$field['title']}}">
                                @else
                                    <textarea class="form-control" name='{{$name}}' id="{{$name}}" placeholder="Enter {{$field['title']}}" rows="3"></textarea>
                                @endif
                                @if ($errors->has($name))
                                <div class="error" style='color: red;'>
                                    {{ $errors->first($name) }}
                                </div>
                                @endif
                            </div>
                        @endforeach
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
