@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Profiles</div>
                <div class="card-body">
                  <div class="d-flex justify-content-end mb-4">
                    @if (Auth::user()->isAdmin())
                        <a class="btn btn-primary" style="margin-right:5px;" href="{{ URL::to('/profiles/create') }}">Create</a>
                    @endif
                    <a class="btn btn-primary" href="{{ URL::to('/pdf/profiles') }}">Export to PDF</a>
                  </div>
                  <table class="table">
                    <thead>
                      <tr>
                        <th scope="col">
                            Name
                        </th>
                        <th scope="col">
                            Position
                        </th>
                        <th scope="col">
                            Skills
                        </th>
                        <th scope="col">
                            Additional info
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach ($profiles as $profile)
                        <tr>
                          <td>
                              {{ $profile->first_name }} {{ $profile->last_name }}
                          </td>
                          <td>
                              {{ $profile->position }}
                          </td>
                          <td>
                              {{ $profile->skills }}
                          </td>
                          <td>
                              {{ $profile->additional_info }}
                          </td>
                        </tr>
                        @endforeach
                    </tbody>
                  </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
