<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>List of Profiles</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
  </head>
  <body>
    <table class="table table-bordered">
      <thead>
          <tr>
            <th scope="col">
                Name
            </th>
            <th scope="col">
                Position
            </th>
            <th scope="col">
                Skills
            </th>
            <th scope="col">
                Additional info
            </th>
          </tr>
      </thead>
      <tbody>
        @foreach ($profiles as $profile)
            <tr>
                <td>
                    {{ $profile->first_name }} {{ $profile->last_name }}
                </td>
                <td>
                    {{ $profile->position }}
                </td>
                <td>
                    {{ $profile->skills }}
                </td>
                <td>
                    {{ $profile->additional_info }}
                </td>
            </tr>
        @endforeach
      </tbody>
    </table>
  </body>
</html>